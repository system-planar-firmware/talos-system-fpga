# Raptor System FPGA General Information

This document contains general information applicable to most Raptor Computing Systems FPGA source trees.  For specific information on a particular product, please consult the provided schematics and FPGA source HDL.


# Building

The system FPGA sources are designed with, and are fully synthesizeable and routable with, the open-source Icestorm tooling.  For more details on Icestorm please visit http://www.clifford.at/icestorm/

## Prerequisites

We recommend using a Debian Buster installation with the non-free repositories disabled.  As part of the build process runs timing-driven PAR, a multi-core system with at least 64 threads is useful to speed up the build process.  We recommend building the FPGA image directly on a decently sized, blob free OpenPOWER system for maximum security and ease of development.

At minimum, the following packages are required:
apt-get install build-essential yosys arachne-pnr fpga-icestorm

If simulation capability is desired, also install:
apt-get install iverilog gtkwave

## Build

Building is simple.  From within the source directory, execute:
make -j &lt;number of threads in build system&gt; all

## Output

When the build is complete, you will see a system_fpga.rom file.  This is the complete image you need to program the Flash device for the FPGA in question on your Raptor Computing Systems product.
