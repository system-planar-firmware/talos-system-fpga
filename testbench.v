// This file is part of the Talos™ II system FPGA implementation
//
// © 2017 - 2019 Raptor Engineering, LLC
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

`timescale 1ns / 100ps

// Behavioral definition of SB_IO primitive
module SB_IO (
		inout PACKAGE_PIN,
		input wire OUTPUT_ENABLE,
		input wire D_OUT_0,
		output wire D_IN_0,
		input wire CLOCK_ENABLE,
		input wire INPUT_CLK,
		input wire OUTPUT_CLK
	);

	parameter PIN_TYPE = 0;
	parameter PULLUP = 0;
	parameter NEG_TRIGGER = 0;

	reg registered_pin;
	reg registered_output_pin;
	wire input_clk_internal;
	wire output_clk_internal;

	assign input_clk_internal = (NEG_TRIGGER == 0) ? INPUT_CLK : ~INPUT_CLK;
	assign output_clk_internal = (NEG_TRIGGER == 0) ? OUTPUT_CLK : ~OUTPUT_CLK;
	always @(posedge input_clk_internal) begin
		registered_pin <= PACKAGE_PIN;
	end
	always @(posedge output_clk_internal) begin
		registered_output_pin <= D_OUT_0;
	end

	assign PACKAGE_PIN = (OUTPUT_ENABLE) ? ((PIN_TYPE[5:2] == 4'b1101) ? registered_output_pin : D_OUT_0) : 1'bz;
	assign D_IN_0 = (PIN_TYPE[1:0] == 2'b01) ? PACKAGE_PIN : registered_pin;

endmodule

// Behavioral definition of SB_GB_IO primitive
module SB_GB_IO (
		inout PACKAGE_PIN,
		input wire OUTPUT_ENABLE,
		input wire D_OUT_0,
		output wire GLOBAL_BUFFER_OUTPUT
	);

	parameter PIN_TYPE = 0;
	parameter PULLUP = 0;

	assign PACKAGE_PIN = (OUTPUT_ENABLE) ? D_OUT_0 : 1'bz;
	assign GLOBAL_BUFFER_OUTPUT = PACKAGE_PIN;

endmodule

// Behavioral definition of SB_GB primitive
module SB_GB (
		input wire USER_SIGNAL_TO_GLOBAL_BUFFER,
		output wire GLOBAL_BUFFER_OUTPUT
	);

	assign GLOBAL_BUFFER_OUTPUT = USER_SIGNAL_TO_GLOBAL_BUFFER;

endmodule

// Behavioral definition of SB_PLL40_CORE primitive
module SB_PLL40_CORE (
		output wire LOCK,
		input wire RESETB,
		input wire BYPASS,
		input wire REFERENCECLK,
		input wire LATCHINPUTVALUE,
		output wire PLLOUTCORE,
		output wire PLLOUTGLOBAL
	);

	parameter FEEDBACK_PATH = 0;
	parameter PLLOUT_SELECT = 0;
	parameter DIVR = 0;
	parameter DIVF = 0;
	parameter DIVQ = 0;
	parameter FILTER_RANGE = 0;
	parameter ENABLE_ICEGATE = 0;

	assign PLLOUTCORE = REFERENCECLK;
	assign PLLOUTGLOBAL = REFERENCECLK;

endmodule

// Main testbench
module system_fpga_test();
	reg lpc_clock;

	wire cpub_clk_oea;
	wire cpub_clk_oeb;

	system_fpga_top U1(
		.lpc_clock(lpc_clock),

		.cpub_clk_oea(cpub_clk_oea),
		.cpub_clk_oeb(cpub_clk_oeb)
	);

	initial begin
		$dumpfile("system_fpga_test.vcd");	// Dumpfile
		$dumpvars;				// Dump all signals
	end

	always
		#15 lpc_clock = ~lpc_clock;		// Main 33MHz clock (approximated)

	initial
		#3000 $finish;				// Terminate after 3µs
endmodule
